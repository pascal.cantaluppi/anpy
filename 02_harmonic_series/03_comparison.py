#!/usr/bin/python3

""" Harmonic series - divergence
"""

import matplotlib.pyplot as plt


# Function for calculating the partial sums of the general harmonic series
def harmonic_series_partial_sum(p, n):
    """
    Calculates the partial sum of the harmonic series up to the nth term.

    :param n: Number of terms to sum
    :return: Partial sum of the first n terms of the harmonic series
    """
    return [sum(1 / i ** p for i in range(1, k + 1)) for k in range(1, n + 1)]


# Number of terms and different p-values
n = 50
p_values = [0.9, 1.0, 1.1]  # p = 1.0 corresponds to the classical harmonic series

# Creating the plot
plt.figure(figsize=(12, 8))

for p in p_values:
    partial_sum = harmonic_series_partial_sum(p, n)
    plt.plot(range(1, n + 1), partial_sum, label=f'p={p}')

plt.xlabel('Number of terms')
plt.ylabel('Sum of the series')
plt.title('Convergence/divergence behavior of the general harmonic series')
plt.legend()
plt.grid(True)
plt.show()
