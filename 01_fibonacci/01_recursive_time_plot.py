#!/usr/bin/python3

""" Fibonacci sequence - recursive runtime plot
"""

import time
import matplotlib.pyplot as plt

sequenceArray = [20, 25, 30, 35]
timeArray = []


def fib(value):
    if value <= 1:
        return value
    else:
        return fib(value - 1) + fib(value - 2)


for number in sequenceArray:
    start_time = time.time()
    for i in range(number):
        fib(i)
    end_time = time.time()
    timeArray.append(end_time - start_time)

print(timeArray)

plt.plot(sequenceArray, timeArray)
plt.show()