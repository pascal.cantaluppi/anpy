#!/usr/bin/python3

import integral.num_integration as numi
sekanten_trapez_regel = numi.sekanten_trapez_regel
tangenten_trapez_regel = numi.tangenten_trapez_regel
simpson_regel = numi.simpson_regel
gauss_quadratur = numi.gauss_quadratur


def f(x):
    return x**2


a = 0
b = 1
n = 100

# 3.1
integral_approx = sekanten_trapez_regel(f, a, b, n)
print("Sekanten Trapez Regel:", integral_approx)

# 3.2
integral_approx = tangenten_trapez_regel(f, a, b, n)
print("Tangenten Trapez Regel: ", integral_approx)

# 3.3
integral_approx = simpson_regel(f, a, b, n)
print("Simpson Regel: ", integral_approx)

# 3.4
integral_approx = gauss_quadratur(f, a, b, n)
print("Gauss Quadratur: ", integral_approx)