#!/usr/bin/python3

""" Fibonacci sequence - memoization technique
"""

import time

count = 0


def fib(value, memo={}):
    if value == 0:
        return 0
    elif value <= 2:
        return 1
    if value not in memo:
        global count
        count += 1
        memo[value] = fib(value - 1) + fib(value - 2)
    return memo[value]


number = int(input("Please enter a number: "))
if number > 0:
    print("Fibonacci sequence to", number)
    start_time = time.time()

    for i in range(number):
        print(fib(i))

    end_time = time.time()
    print("Runtime: ", end_time - start_time)
    print("Recursions: ", count)

else:
    print("Not a positive integer")
