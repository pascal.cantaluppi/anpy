#!/usr/bin/python3

""" Fibonacci sequence - iterative runtime plot
"""

import time
import matplotlib.pyplot as plt


sequenceArray = [1000, 2000, 3000, 4000]
timeArray = []

for number in sequenceArray:
    start_time = time.time()
    n1, n2 = 0, 1
    for i in range(number):
        nth = n1 + n2
        n1 = n2
        n2 = nth
    end_time = time.time()
    timeArray.append(end_time - start_time)

print(timeArray)

plt.plot(sequenceArray, timeArray)
plt.show()
