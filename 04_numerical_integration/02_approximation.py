#!/usr/bin/python3

import numpy as np
import integral.num_integration as numi
sekanten_trapez_regel = numi.sekanten_trapez_regel
tangenten_trapez_regel = numi.tangenten_trapez_regel
simpson_regel = numi.simpson_regel
gauss_quadratur = numi.gauss_quadratur


# 4.1
print(sekanten_trapez_regel(lambda x: np.pi*1/x,1,2,1))
print(tangenten_trapez_regel(lambda x: np.pi*1/x,1,2,1))
print(simpson_regel(lambda x: np.pi*1/x,1,2,1))
print(gauss_quadratur(lambda x: np.pi*1/x, 1, 2, 2))

print("-")

# 4.2
print(sekanten_trapez_regel(lambda x: x**3+3*x**2,1,2,2))
print(tangenten_trapez_regel(lambda x: x**3+3*x**2,1,2,2))
print(simpson_regel(lambda x: x**3+3*x**2,1,2,2))
print(gauss_quadratur(lambda x: x**3 + 3*x**2, 1, 2, 2))

print("-")

# 4.4
print(sekanten_trapez_regel(lambda x: np.cos(x), ((-1)*np.pi/2),np.pi/2, 3))
print(tangenten_trapez_regel(lambda x: np.cos(x), ((-1)*np.pi/2),np.pi/2, 3 ))
print(simpson_regel(lambda x: np.cos(x), ((-1)*np.pi/2),np.pi/2, 3 ))
print(gauss_quadratur(lambda x: np.cos(x), (-1)*np.pi/2, np.pi/2, 3))