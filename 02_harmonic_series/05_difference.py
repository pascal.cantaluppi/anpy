# !/usr/bin/python3

""" Harmonic series - difference between consecutive partial sums
"""

import matplotlib.pyplot as plt


def harmonic_series_partial_sum(n):
    """
    Calculates the partial sum of the harmonic series up to the nth term.

    :param n: Number of terms to sum
    :return: Partial sum of the first n terms of the harmonic series
    """
    return sum(1 / i for i in range(1, n + 1))


n_max = 100
partial_sums = [harmonic_series_partial_sum(n) for n in range(1, n_max + 1)]
differences = [partial_sums[i] - partial_sums[i - 1] for i in range(1, n_max)]

plt.figure(figsize=(10, 6))
plt.plot(range(2, n_max + 1), differences)
plt.title('Difference between consecutive partial sums of the harmonic series')
plt.xlabel('Number of links (n)')
plt.ylabel('Difference in subtotals')
plt.grid(True)
plt.show()
