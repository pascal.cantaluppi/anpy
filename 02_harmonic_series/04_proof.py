#!/usr/bin/python3

""" Harmonic series - proof of divergence
"""

import sympy as sp

# Defining the variable
n = sp.symbols('n')

# Defining the harmonic series
harmonic_series = sp.Sum(1/n, (n, 1, sp.oo))

# Checking for convergence
convergence_result = harmonic_series.is_convergent()

# Print the results
GREEN = '\033[92m'
RESET = '\033[0m'
print("Convergence Result: " + GREEN + str(convergence_result) + RESET)
print("Harmonic Series:", harmonic_series)
                                                 