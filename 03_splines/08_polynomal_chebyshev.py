#!/usr/bin/python3

""" Splines - polynomial with chebyshev
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.polynomial import Polynomial

# Generate sample data
x = np.array([1, 2, 3, 4, 5, 6])
y = np.array([1, 4, 9, 16, 25, 36])  # Quadratic relationship y = x^2

# Fit a polynomial of third degree
degree = 3
p = Polynomial.fit(x, y, degree)

# Evaluate the fitted polynomial at many points to create a smooth curve
x_new = np.linspace(x[0], x[-1], 100)
y_new = p(x_new)

# Plot the original data and the fitted curve
plt.figure(figsize=(8, 4))
plt.plot(x, y, 'o', label='Original data')
plt.plot(x_new, y_new, '-', label='Fitted polynomial')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Polynomial Fitting with numpy.polynomial.Polynomial')
plt.legend()
plt.grid(True)
plt.show()
