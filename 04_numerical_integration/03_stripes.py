#!/usr/bin/python3

import numpy as np
import integral.num_integration as numi
sekanten_trapez_regel = numi.sekanten_trapez_regel
tangenten_trapez_regel = numi.tangenten_trapez_regel
simpson_regel = numi.simpson_regel
gauss_quadratur = numi.gauss_quadratur


# 4.1
for i in range(10, 51, 10):
    print(sekanten_trapez_regel(lambda x: np.cos(x), ((-1) * np.pi / 2), np.pi / 2, i))

print("-")

# 4.2
for i in range(10, 51, 10):
    print(tangenten_trapez_regel(lambda x: np.cos(x), ((-1)*np.pi/2),np.pi/2, i ))

print("-")

# 4.3
for i in range(10, 51, 10):
    print(simpson_regel(lambda x: np.cos(x), ((-1) * np.pi / 2), np.pi / 2, 1))

print("-")

# 4.4
for i in range(10, 51, 10):
    print(gauss_quadratur(lambda x: np.cos(x), (-1) * np.pi / 2, np.pi / 2, i))