#!/usr/bin/python3

""" Fibonacci sequence - bottom up method
"""

import time


def fib(value):
    if value == 0:
        return 0
    elif value <= 2:
        return 1
    bottom_up = [0, 1, 1] + [0] * (value - 2)
    for i in range(3, value + 1):
        bottom_up[i] = bottom_up[i - 1] + bottom_up[i - 2]
    return bottom_up[value]


number = int(input("Please enter a number: "))
if number > 0:
    print("Fibonacci sequence to", number)
    start_time = time.time()

    for i in range(number):
        print(fib(i))

    end_time = time.time()
    print("Runtime: ", end_time - start_time)

else:
    print("Not a positive integer")
