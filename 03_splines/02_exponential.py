#!/usr/bin/python3

""" Splines - exponential functions
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# Generate data
x = np.array([1, 2, 3, 4, 5])
y = np.array([2.718, 7.389, 20.086, 54.598, 148.413])

# Construct splines
spl = interp1d(x, y, kind='cubic', fill_value="extrapolate")
x_new = np.linspace(1, 10, 1000)
y_new = spl(x_new)

# plot the spines
plt.plot(x, y, 'o', label='Datenpunkte')
plt.plot(x_new, y_new, label='Spline')
plt.legend(loc='best')
plt.show()
