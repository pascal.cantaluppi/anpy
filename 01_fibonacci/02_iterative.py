#!/usr/bin/python3

""" Fibonacci sequence - iterative algorithm
"""

import time

number = int(input("Please enter a number: "))
n1, n2 = 0, 1
if number <= 0:
    print("Not a positive integer")
else:
    print("Fibonacci sequence to", number)

    start_time = time.time()

    for i in range(number):
        print(n1)
        nth = n1 + n2
        n1 = n2
        n2 = nth

    end_time = time.time()
    print("Runtime: ", end_time - start_time)