#!/usr/bin/python3

""" Fibonacci sequence - using generator
"""

import time
import itertools


def fib():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


number = int(input("Please enter a number: "))
if number > 0:
    print("Fibonacci sequence to", number)
    start_time = time.time()

    fib_seq = itertools.islice(fib(), number)
    for num in fib_seq:
        print(str(num))

    end_time = time.time()
    print("Runtime: ", end_time - start_time)

else:
    print("I need a positive integer")
