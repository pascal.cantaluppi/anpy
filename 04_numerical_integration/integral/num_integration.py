#!/usr/bin/python3

""" Numerical integration
"""

import numpy as np


def sekanten_trapez_regel(f, a, b, n):
    """
    Computes the integral of the function f using the secant trapezoid rule with n subintervals.

    :param f: the function to integrate
    :param a: the lower limit of integration
    :param b: the upper limit of integration
    :param n: the number of subintervals
    :return: the approximate value of the integral
    """
    h = (b - a) / n  # Step size

    # Compute the sum of function values at the support points
    sum = 0
    for i in range(1, n):
        xi = a + i * h
        sum += f(xi)

    # Compute the integral
    integral = h * (0.5 * f(a) + 0.5 * f(b) + sum)

    return integral


def tangenten_trapez_regel(f, a, b, n):
    """
    Computes the integral of the function f using the tangent trapezoid rule with n subintervals.

    :param f: the function to integrate
    :param a: the lower limit of integration
    :param b: the upper limit of integration
    :param n: the number of subintervals
    :return: the approximate value of the integral
    """
    h = (b - a) / n  # Step size

    # Compute the sum of function values at the support points
    sum = 0
    for i in range(n):
        xi = a + i * h
        sum += f(xi) + f(xi + h)

    # Compute the integral
    integral = h * 0.5 * sum

    return integral


def simpson_regel(f, a, b, n):
    """
    Computes the integral of the function f using Simpson's rule with n subintervals.

    :param f: the function to integrate
    :param a: the lower limit of integration
    :param b: the upper limit of integration
    :param n: the number of subintervals (must be an even integer)
    :return: the approximate value of the integral
    """
    h = (b - a) / n  # Calculation of step size
    x = a  # Starting point
    result = 0  # Initializing the result

    for i in range(n):
        result += f(x) + 4 * f(x + h / 2) + f(x + h)  # Simpson's rule formula
        x += h

    result *= h / 6  # Multiplication by step size and factor 1/6

    return result


def gauss_quadratur(f, a, b, n):
    """
    Computes the integral of the function f using Gaussian quadrature with n points.

    :param f: the function to integrate
    :param a: the lower limit of integration
    :param b: the upper limit of integration
    :param n: the number of points
    :return: the approximate value of the integral
    """
    # Gauss-Legendre (n points)
    [x, w] = np.polynomial.legendre.leggauss(n)

    # Change of interval from [a, b] to [-1, 1]
    t = 0.5 * (x + 1) * (b - a) + a
    integral = 0.5 * (b - a) * np.sum(w * f(t))

    return integral