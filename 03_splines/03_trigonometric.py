#!/usr/bin/python3

""" Splines - trigonometric functions
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# Generate data
x = np.array([0, 1, 2, 3, 4, 5])
y = np.array([0, 0.5, 0.866, 1, 0.866, 0.5])

# Construct splines
spl = interp1d(x, y, kind='cubic', fill_value="extrapolate")
x_new = np.linspace(0, 6, 1000)
y_new = spl(x_new)

# plot the spines
plt.plot(x, y, 'o', label='Datenpunkte')
plt.plot(x_new, y_new, label='Spline')
plt.legend(loc='best')
plt.show()
