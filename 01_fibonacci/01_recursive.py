#!/usr/bin/python3

""" Fibonacci sequence - recursive algorithm
"""

import time

count = 0


def fib(value):
    global count
    count += 1
    if value <= 1:
        return value
    else:
        return fib(value - 1) + fib(value - 2)


number = int(input("Please enter a number: "))
if number > 0:
    print("Fibonacci sequence to", number)
    start_time = time.time()

    for i in range(number):
        print(fib(i))

    end_time = time.time()
    print("Runtime: ", end_time - start_time)
    print("Recursions: ", count)

else:
    print("Not a positive integer")
