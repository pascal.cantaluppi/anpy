#!/usr/bin/python3

""" Splines - Smoothing univariate
"""


import numpy as np
from scipy.interpolate import splrep, BSpline

# Generate noisy data
x = np.arange(0, 2*np.pi+np.pi/4, 2*np.pi/16)
rng = np.random.default_rng()
y =  np.sin(x) + 0.4*rng.standard_normal(size=len(x))

# Construct splines
tck = splrep(x, y, s=0)
tck_s = splrep(x, y, s=len(x))

# Plot the spines
import matplotlib.pyplot as plt
xnew = np.arange(0, 9/4, 1/50) * np.pi
plt.plot(xnew, np.sin(xnew), '-.', label='sin(x)')
plt.plot(xnew, BSpline(*tck)(xnew), '-', label='s=0')
plt.plot(xnew, BSpline(*tck_s)(xnew), '-', label=f's={len(x)}')
plt.plot(x, y, 'o')
plt.legend()
plt.show()