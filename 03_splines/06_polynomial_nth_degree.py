#!/usr/bin/python3

""" Splines - polynomials of degree n
"""

import numpy as np
from scipy.interpolate import UnivariateSpline

# Generate data
x = np.array([0, 1, 2, 3, 4, 5])
y = np.array([0, 2, 1, 3, 2, 4])

# Construct splines
n = 3 # Degree
spl = UnivariateSpline(x, y, k=n)

xnew = np.linspace(0, 5, 1000)

# Polynomial of degree n
import matplotlib.pyplot as plt
plt.plot(xnew, spl(xnew))
plt.show()
