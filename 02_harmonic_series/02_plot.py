#!/usr/bin/python3

""" Harmonic series - partial sum plot
"""


import matplotlib.pyplot as plt

def harmonic_series_partial_sum(n):
    """
    Calculates the partial sum of the harmonic series up to the nth term.

    :param n: Number of terms to sum
    :return: Partial sum of the first n terms of the harmonic series
    """
    partial_sum = 0
    for i in range(1, n + 1):
        partial_sum += 1 / i
    return partial_sum

# Generate values for n
n_values = list(range(1, 11))  # For example, from 1 to 10

# Calculate partial sums for each value of n
partial_sums = [harmonic_series_partial_sum(n) for n in n_values]

# Create the plot
plt.plot(n_values, partial_sums, marker='o')
plt.xlabel('n')
plt.title('Partial Sum of Harmonic Series for Various n')
plt.grid(True)
plt.show()
