#!/usr/bin/python3

""" Fibonacci sequence - binet's formula
"""

import time
import math


def fib(value):
    return round(((1 + math.sqrt(5)) ** value - (1 - math.sqrt(5)) ** value) / (2 ** value * math.sqrt(5)))


number = int(input("Please enter a number: "))
if number > 0:
    print("Fibonacci sequence to", number)
    start_time = time.time()

    for i in range(number):
        print(fib(i))

    end_time = time.time()
    print("Runtime: ", end_time - start_time)

else:
    print("Not a positive integer")