#!/usr/bin/python3

""" Splines - polynomials
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# Generate data
x = np.array([1, 2, 3, 4, 5])
y = np.array([3, 1, 4, 2, 5])

# Construct splines
spl = interp1d(x, y, kind='cubic')
x_new = np.linspace(1, 5, 1000)
y_new = spl(x_new)

# plot the spines
plt.plot(x, y, 'o', label='Datenpunkte')
plt.plot(x_new, y_new, label='Spline')
plt.legend(loc='best')
plt.show()
