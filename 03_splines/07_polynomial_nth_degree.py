#!/usr/bin/python3

""" Splines - polynomial of the nth degree
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# Generate data
x = np.array([1, 2, 3, 4, 5])
y = np.array([3, 1, 4, 2, 5])

# Construct cubic spline
cubic_spline = interp1d(x, y, kind='cubic')
x_new = np.linspace(1, 5, 1000)
y_cubic = cubic_spline(x_new)

# Construct polynomial interpolation
polynomial_interpolation = interp1d(x, y, kind='linear')  # 'linear' for simple polynomials
y_poly = polynomial_interpolation(x_new)

# Plotting
plt.plot(x, y, 'o', label='Datenpunkte')
plt.plot(x_new, y_cubic, label='Kubischer Spline')
plt.plot(x_new, y_poly, label='Polynominterpolation')
plt.legend(loc='best')
plt.show()
